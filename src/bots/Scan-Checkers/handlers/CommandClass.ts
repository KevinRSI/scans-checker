export class Interact {
  name: string;
  description: string;

  public constructor(name: string, desc: string) {
    (this.name = name), (this.description = desc);
  }
}
