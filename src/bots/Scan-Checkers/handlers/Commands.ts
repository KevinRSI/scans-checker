import { Message } from "discord.js";
import { Interact } from "./CommandClass";
import { defaultFunc } from "./Functions/default";
import defaultChannel from "./Functions/defaultChannel";
import help from "./Functions/help";
import list from "./Functions/list";
import { sayPong } from "./Functions/Pong";
import scanCheck from "./Functions/scanCheck";
import timeBomb from "./Functions/timeBomb";

export const prefix = "!!";

export const commands: Interact[] = [
  { name: prefix + "help", description: "show the list of commands" },

  { name: prefix + "ping", description: "say pong back" },
  {
    name: prefix + "list",
    description: "Add a Mangadex list to watch",
  },
  {
    name: prefix + "check",
    description: "check scans out last 24hrs",
  },
  {
    name: prefix + "channel",
    description: "Set my default channel on this server",
  },
];

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function switchCommands(message: Message) {
  switch (message.content.split(" ")[0].replace("!!", "")) {
    case "ping":
      sayPong(message);
      break;
    case "check":
      scanCheck(message);
      break;
    case "help":
      help(message);
      break;
    case "list":
      list(message);
      break;
    case "channel":
      defaultChannel(message);
      break;
    default:
      defaultFunc(message);
      break;
  }
}
