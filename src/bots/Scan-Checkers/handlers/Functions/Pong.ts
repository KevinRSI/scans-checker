import { Message } from "discord.js";

export function sayPong(message: Message): Promise<Message> {
  return message.reply("Pong !");
}
