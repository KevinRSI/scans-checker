import { Message } from "discord.js";

export const defaultFunc = (message: Message): Promise<Message> => {
  return message.reply("please set a default channel using !!channel");
};
