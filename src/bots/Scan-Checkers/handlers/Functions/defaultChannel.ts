import { Message } from "discord.js";
import { server } from "../../../..";

const defaultChannel = async (message: Message) => {
  if (!message.member?.permissions.has("ADMINISTRATOR")) {
    message.reply("You need to be administrator to do that");
    return;
  }
  const instance = server.db("ScanCheck").collection("Servers");

  const thisServer = await instance
    .findOne({ serverId_1: message.guildId })
    .catch((error) => console.log(error));

  if (thisServer?.serverId_1 === message.guildId) {
    let update = await instance
      .updateOne(
        { serverId_1: message.guildId },
        { $set: { channel: message.channelId } }
      )
      .catch((error) => console.log(error));

    if (update && update.acknowledged) {
      message.react("✅");
    }
  } else {
    const insert = await server
      .db("ScanCheck")
      .collection("Servers")
      .insertOne({
        serverId_1: message.guildId,
        channel: message.channelId,
      })
      .catch((error) => console.log(error));

    if (insert && insert.acknowledged) {
      message.react("✅");
      message.channel.send("You can now use !!list to set your mangadex list");
    }
  }
};
export default defaultChannel;
