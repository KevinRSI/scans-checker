import { Message, MessageEmbed } from "discord.js";
import { discordClient } from "../../../../client";
import { commands } from "../Commands";

export default async function help(message: Message): Promise<void> {

  const embed = new MessageEmbed()
    .setTitle("Help")
    .setDescription("See the list of commands")
    .setAuthor({
      name: "Scans-Checker",
      iconURL: discordClient.user?.avatarURL() as string | undefined,
    })
	.setColor("#ff725c")
    .setFooter({ text: 'you need to type "!!" in front of any commands' });

  commands.map((item) => {
    if (!item.name.includes("help")) {
      embed.addField(item.name.slice(2), item.description, false);
    }
  });

  message.channel.send({ embeds: [embed] });
}
// https://api.mangadex.org/list/80fcf689-ab63-4e84-8566-8ece48004fe4/feed?translatedLanguage[]=en&includes[]=manga
