import axios from "axios";
import { Message, MessageReaction, User } from "discord.js";
import { server } from "../../../..";
export const countries = [
  { emoji: "🇺🇸", lang: "en" },
  { emoji: "🇫🇷", lang: "fr" },
  { emoji: "🇯🇵", lang: "jp" },
  { emoji: "🇮🇹", lang: "it" },
];

export default async function list(message: Message): Promise<void> {
	if (!message.member?.permissions.has("ADMINISTRATOR")) {
		message.reply("You need to be administrator to do that");
		return;
	  }
  const regex = new RegExp(
    /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i
  );
  const splitMessage = message.content.split(" ");
  if (splitMessage.length > 1) {
    const id = splitMessage
      .filter((item) => item.startsWith("https"))[0]
      .replace("https://mangadex.org/list/", "")
      .split("/")[0];

    if (id.match(regex)) {
		try {
			const {status} = await axios
			.get("https://api.mangadex.org/list/" + id, {
			  headers: {
				"Cache-Control": "no-cache",
				Pragma: "no-cache",
				Expires: "0",
			  },
			})
			
		  const reply = await message.channel.send({
			content:
			  "In which language should I fetch the new chapters ? \nPlease use the reaction to choose",
			target: message.author,
			isInteraction: true,
		  });
		  Promise.all([
			countries.forEach((element) => {
			  reply.react(element.emoji);
			}),
		  ]).catch((error) =>
			console.error("One of the emojis failed to react:", error)
		  );
		  const filter = (reaction: MessageReaction, user: User) => {
			return (
			  countries.filter(
				(item) => item.emoji === (reaction?.emoji?.name as string)
			  ).length > 0 && user.id === message.author.id
			);
		  };
		  const reaction = await reply
			.awaitReactions({
			  filter,
			  max: 1,
			  time: 60000,
			})
			.then(async (collected) => {
			  const reaction = collected.first();
	
			  if (
				reaction?.emoji.name &&
				countries.filter(
				  (item) => item.emoji === (reaction?.emoji?.name as string)
				).length > 0
			  ) {
				const insert = await server
				  .db("ScanCheck")
				  .collection("Servers")
				  .updateOne(
					{
					  serverId_1: message.guildId,
					},
					{
					  $set: {
						list: id,
						lang: reaction.emoji.name,
					  },
					}
				  );
				if (insert.acknowledged) {
				  message.reply(
					"Configuration complete, if you made an error or wish to update it later, please use !!list again"
				  );
				}
			  } else {
				message.reply("You reacted with a wrong emoji");
			  }
			})
			.catch((collected) => {
			  message.reply("an error happened");
			});
		} catch (error) {
			message.reply("Please set your list visibility to public");
			return;
		}
     
    } else {
      message.reply(
        "Wrong list, exemple : https://mangadex.org/list/80fcf689-ab63-4e84-8566-8ece48004fe2/nameOfYourList"
      );
      return;
    }
  } else {
    message.reply(
      "Wrong list, exemple : https://mangadex.org/list/80fcf689-ab63-4e84-8566-8ece48004fe2/nameOfYourList"
    );
    return;
  }
}
