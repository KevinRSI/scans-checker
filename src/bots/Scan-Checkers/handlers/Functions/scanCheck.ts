import {
  Channel,
  Guild,
  GuildBasedChannel,
  Message,
  MessageEmbed,
  TextChannel,
} from "discord.js";
import { server } from "../../../..";
import axios from "axios";
import { countries } from "./list";
import { UUID } from "bson";
interface Data {
  result: string;
  response: string;
  data: {
    id: UUID;
    type: string;
    attributes: {
      volume: string | null;
      chapter: string;
      title: string;
      translatedLanguage: string;
      externalUrl: string | null;
      publishAt: string;
      readableAt: string;
      createdAt: string;
      updatedAt: string;
      pages: number;
      version: number;
    };
    relationships: any[];
  }[];
}
export default async function scanCheck(
  message?: Message,
  channel?: GuildBasedChannel
): Promise<void> {
  try {
    const guild = await server
      ?.db("ScanCheck")
      .collection("Servers")
      .findOne({ serverId_1: message ? message?.guildId : channel?.guildId });

    if (guild && guild.list) {
      const lang = countries.filter((item) => item.emoji === guild.lang);
      const date = new Date();
      date.setDate(date.getDate() - 1);
      if (lang.length && date) {
        const { data } = await axios.get(
          `https://api.mangadex.org/list/${
            guild.list
          }/feed?translatedLanguage[]=${
            lang[0].lang
          }&includes[]=scanlation_group&updatedAtSince=${
            date.toISOString().split(".")[0]
          }`
        );

        if (data?.data) {
          let embeds: MessageEmbed[] = [];
          (data as Data).data.forEach(async (item) => {
            const mangaId = item?.relationships.filter(
              (item) => item.type === "manga"
            )[0].id;

            const embed = await createEmbed(item, mangaId);
            if (embed) {
              embeds.push(embed);
            }
          });

          const send = setInterval(() => {
            if (
              data.data.length > 0 &&
              data.data.length === embeds.length &&
              !channel &&
              message
            ) {
              message.channel.send({ embeds: embeds });
              clearInterval(send);
              return;
            } else if (
              data.data.length > 0 &&
              data.data.length === embeds.length &&
              channel &&
              !message
            ) {
              (channel as TextChannel).send({ embeds: embeds });
              clearInterval(send);
              return;
            } else {
              if (channel && !message) {
                (channel as TextChannel).send("Nothing new was found today !");
                clearInterval(send);
                return;
              } else {
                message?.channel.send("Nothing new was found today !");
                clearInterval(send);
                return;
              }
            }
          }, 500);

          return;
        }
      }
    }
  } catch (error) {
    console.log(error);
    return;
  }
  return;
}
// https://api.mangadex.org/list/80fcf689-ab63-4e84-8566-8ece48004fe4/feed?translatedLanguage[]=en&includes[]=manga&updatedAtSince=2022-02-22T00:00:00

const createEmbed = async (item: any, mangaId: string) => {
  const manga = await axios.get(
    "https://api.mangadex.org/manga/" + mangaId + "?includes[]=cover_art"
  );
  const picture = manga.data.data.relationships.filter(
    (item: { type: string; id: string }) => item.type === "cover_art"
  )[0].attributes.fileName;

  const titles = manga.data.data.attributes.title;
  const sclName = item.relationships.filter(
    (item: any) => item.type === "scanlation_group"
  )[0].attributes.name;

  const embed = new MessageEmbed()
    .setTitle(Object.values(titles)[0] as string)
    .setAuthor({ name: sclName ?? "" })
    .addField(item.attributes.title, "Chapter " + item.attributes.chapter)
    .setThumbnail(
      "https://uploads.mangadex.org/covers/" +
        mangaId +
        "/" +
        picture +
        ".256.jpg"
    )
    .setURL("https://mangadex.org/chapter/" + item?.id)
    .setColor("#FFA07A");
  return embed;
};
