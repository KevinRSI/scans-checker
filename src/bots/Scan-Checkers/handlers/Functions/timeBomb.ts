import { GuildBasedChannel, Message, TextChannel } from "discord.js";
import { server } from "../../../..";
import { discordClient } from "../../../../client";
import scanCheck from "./scanCheck";

const timeBomb = async () => {
  const servers = server.db("ScanCheck").collection("Servers").find({});
  servers.forEach((item) => {
    const channel = discordClient.channels.cache.get(item.channel);
    if (channel) {
      scanCheck(undefined, channel as GuildBasedChannel)
    }
  });
};

export default timeBomb;
