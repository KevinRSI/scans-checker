import { Message, TextChannel } from "discord.js";
import Discord from "discord.js";
import { Intents } from "discord.js";
import {
  commands,
  switchCommands,
} from "./bots/Scan-Checkers/handlers/Commands";
import { defaultFunc } from "./bots/Scan-Checkers/handlers/Functions/default";
import { prefix } from "./bots/Scan-Checkers/handlers/Commands";
import { server } from ".";
import timeBomb from "./bots/Scan-Checkers/handlers/Functions/timeBomb";
import { scheduleJob, RecurrenceRule } from "node-schedule";
export const discordClient = new Discord.Client({
  intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_MEMBERS,
    Intents.FLAGS.GUILD_INTEGRATIONS,
    Intents.FLAGS.GUILD_WEBHOOKS,
    Intents.FLAGS.GUILD_VOICE_STATES,
    Intents.FLAGS.GUILD_PRESENCES,
    Intents.FLAGS.GUILD_MESSAGES,
    Intents.FLAGS.GUILD_MESSAGE_REACTIONS,
  ],
});

discordClient.on("messageCreate", async (message: Message) => {
  if (message.author.bot) return;
  if (message.content.startsWith(prefix)) {
    const instance = await server
      .db("ScanCheck")
      .collection("Servers")
      .findOne({ serverId_1: message.guildId });
    const command = commands.filter(
      (item) => item.name === message.content.split(" ")[0]
    );
    if (
      command.length > 0 &&
      (command[0].name === "!!channel" || instance?._id)
    ) {
      switchCommands(message);
    } else if (command.length === 0) {
      return;
    } else {
      defaultFunc(message);
      return;
    }
  }
});

discordClient.on("guildCreate", async (guild) => {
  let defaultChannel: TextChannel;
  guild.channels.cache.forEach((channel) => {
    if (channel.type == "GUILD_TEXT") {
      if (guild.me && channel.permissionsFor(guild.me).has("SEND_MESSAGES")) {
        defaultChannel = channel as TextChannel;
        defaultChannel.send(
          "Hello, Im Scan Checkers ! \nPlease use !!help to see the commands"
        );
        return;
      }
      return;
    }
    return;
  });
});

export const launch = (): void => {
  discordClient.on("ready", () => {
    if (discordClient) {
      console.log(`Logged in as ${discordClient.user?.tag}!`);
      let rule = new RecurrenceRule();
      rule.hour = 18;
	  rule.minute = 1;
	  rule.second = 0;
      scheduleJob("scan-check", rule, () => {
        timeBomb();
      });
	  
    }
  });

  discordClient.login(process.env.DISCORD_BOT_TOKEN);
};
