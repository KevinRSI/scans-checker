import "dotenv/config";
import { discordClient, launch } from "./client";
import { MongoClient } from "mongodb";
import { cancelJob } from "node-schedule";

export const server = new MongoClient(process.env.DATABASE_URL as string);

server.connect(async (err): Promise<void> => {
  if (err) throw err;

  // Start the application after the database connection is ready
  launch();
});

process.on("SIGINT", () => {
  console.log(" \nexiting");
  server.close();
  cancelJob("scan-check");
  discordClient.destroy();
  process.exit(0);
});

process.on("SIGTERM", function () {
  console.log("Stopping server");
  server.close();
  cancelJob("scan-check")
  discordClient.destroy();
  process.exit(0);
});
