export interface IFilters {
  serverId?: string;
  listId?: string;
}

export interface IServers {
  document: {
    _id: string;
    serverId_1: string;
	list?: string;
	lang?: string;
	channel: string
  };
}
